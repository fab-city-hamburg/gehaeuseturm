[Beschaffungstabelle](https://docs.google.com/spreadsheets/d/16fBucCqLqE4dzYUjHzPDkWTic6IFBzdLavR26Joye6Q/edit?usp=sharing)

**Reichelt: (elektronische Komponenten)**
https://www.reichelt.de/my/1800883
Der Link führt direkt auf eine Zusammenstellung im Shop

**MediaMarkt / Saturn: (Aktivkohlefilter)**
1x https://www.mediamarkt.de/de/product/_pkm-cf-160-1969240.html

**IKEA: (Korpus)**
3x https://www.ikea.com/de/de/p/lack-beistelltisch-schwarz-20011408/

**Plattenzuschnitt24: (Acryl)**
https://www.plattenzuschnitt24.de
3x Acrylglas XT farblos (44x44cm Kanten gesägt)
2x Acrylglas XT farblos (22x44cm Kanten gesägt)

**EBay (Rollen über: preis-held):**
1x https://www.ebay.de/itm/Transportrollen-Set-4-x-Lenkrollen-Bockrollen-Vollgummi-Schwerlastrollen-Mobel/174339979439
Rollenart: Orange Kugellager (4x Rollen) Durchmesser 40mm

**EBay (Magnetplatten):**
https://www.ebay.de/itm/Neodym-Magnete-flach-20-x-6-x-2mm-N38SH-8-Stuck/303590166436
